# README #

This Exploratory data analysis was done in preperation for the course "Introduction Machine Learning 2020-2021" at the Hanze University Groningen.  
In this EDA we take a look at the differing lengths of treatment for drug abuse based on the drug a patient uses. The Rmd file also contains some code that is used to generate data for the Java program that can be found here. https://bitbucket.org/jdbecirspahic/bfv3_thema09_practicum_jelle_becirspahic/src/master/

# contact
name: Jelle Becirspahic  
email: j.d.becirspahic@st.hanze.nl  
studentnumber: 387615  